From 4e1a4eb28f0e476cf4587d56b5cef4350b33ab82 Mon Sep 17 00:00:00 2001
From: Wenkai Lin <linwenkai6@hisilicon.com>
Date: Fri, 29 Mar 2024 16:53:04 +0800
Subject: [PATCH 45/52] cipher: optimze input lengths check

It is more reasonable to check the input lengths of various cipher
algorithms at the algorithm layer.

Signed-off-by: Wenkai Lin <linwenkai6@hisilicon.com>
Signed-off-by: Qi Tao <taoqi10@huawei.com>
---
 drv/hisi_sec.c | 19 +++++--------------
 wd_cipher.c    | 26 ++++++++++++++++++++++++++
 2 files changed, 31 insertions(+), 14 deletions(-)

diff --git a/drv/hisi_sec.c b/drv/hisi_sec.c
index 852340d..6625c41 100644
--- a/drv/hisi_sec.c
+++ b/drv/hisi_sec.c
@@ -960,10 +960,9 @@ static void parse_cipher_bd2(struct hisi_qp *qp, struct hisi_sec_sqe *sqe,
 		dump_sec_msg(temp_msg, "cipher");
 }
 
-static int aes_sm4_len_check(struct wd_cipher_msg *msg)
+static int aes_len_check(struct wd_cipher_msg *msg)
 {
-	if (msg->alg == WD_CIPHER_AES &&
-	    msg->in_bytes <= AES_BLOCK_SIZE &&
+	if (msg->in_bytes <= AES_BLOCK_SIZE &&
 	    (msg->mode == WD_CIPHER_CBC_CS1 ||
 	     msg->mode == WD_CIPHER_CBC_CS2 ||
 	     msg->mode == WD_CIPHER_CBC_CS3)) {
@@ -972,13 +971,6 @@ static int aes_sm4_len_check(struct wd_cipher_msg *msg)
 		return -WD_EINVAL;
 	}
 
-	if ((msg->in_bytes & (AES_BLOCK_SIZE - 1)) &&
-	    (msg->mode == WD_CIPHER_CBC || msg->mode == WD_CIPHER_ECB)) {
-		WD_ERR("failed to check input bytes of AES or SM4, size = %u\n",
-			msg->in_bytes);
-		return -WD_EINVAL;
-	}
-
 	return 0;
 }
 
@@ -986,8 +978,7 @@ static int cipher_len_check(struct wd_cipher_msg *msg)
 {
 	int ret;
 
-	if (msg->in_bytes > MAX_INPUT_DATA_LEN ||
-	    !msg->in_bytes) {
+	if (msg->in_bytes > MAX_INPUT_DATA_LEN) {
 		WD_ERR("input cipher length is error, size = %u\n",
 		       msg->in_bytes);
 		return -WD_EINVAL;
@@ -1016,8 +1007,8 @@ static int cipher_len_check(struct wd_cipher_msg *msg)
 		return 0;
 	}
 
-	if (msg->alg == WD_CIPHER_AES || msg->alg == WD_CIPHER_SM4) {
-		ret = aes_sm4_len_check(msg);
+	if (msg->alg == WD_CIPHER_AES) {
+		ret = aes_len_check(msg);
 		if (ret)
 			return ret;
 	}
diff --git a/wd_cipher.c b/wd_cipher.c
index f35ce6f..279ca8b 100644
--- a/wd_cipher.c
+++ b/wd_cipher.c
@@ -565,6 +565,28 @@ static int cipher_iv_len_check(struct wd_cipher_req *req,
 	return ret;
 }
 
+static int cipher_len_check(handle_t h_sess, struct wd_cipher_req *req)
+{
+	struct wd_cipher_sess *sess = (struct wd_cipher_sess *)h_sess;
+
+	if (!req->in_bytes) {
+		WD_ERR("invalid: cipher input length is zero!\n");
+		return -WD_EINVAL;
+	}
+
+	if (sess->alg != WD_CIPHER_AES && sess->alg != WD_CIPHER_SM4)
+		return 0;
+
+	if ((req->in_bytes & (AES_BLOCK_SIZE - 1)) &&
+	    (sess->mode == WD_CIPHER_CBC || sess->mode == WD_CIPHER_ECB)) {
+		WD_ERR("failed to check input bytes of AES or SM4, size = %u\n",
+		       req->in_bytes);
+		return -WD_EINVAL;
+	}
+
+	return 0;
+}
+
 static int wd_cipher_check_params(handle_t h_sess,
 				struct wd_cipher_req *req, __u8 mode)
 {
@@ -587,6 +609,10 @@ static int wd_cipher_check_params(handle_t h_sess,
 		return -WD_EINVAL;
 	}
 
+	ret = cipher_len_check(h_sess, req);
+	if (unlikely(ret))
+		return ret;
+
 	ret = wd_check_src_dst(req->src, req->in_bytes, req->dst, req->out_bytes);
 	if (unlikely(ret)) {
 		WD_ERR("invalid: src/dst addr is NULL when src/dst size is non-zero!\n");
-- 
2.25.1

